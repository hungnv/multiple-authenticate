@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <a class="btn btn-info" href="{{url("/posts/new")}}">New</a>
                <table class="table table-borded">
                    <thead>
                    <tr>
                        <th>Content</th>
                        <th>User</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <th>{{$post->content}}</th>
                            <th>{{$post->user->name}}</th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

