@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <a href="{{url("/posts")}}">Post list</a>
                <form action="{{route("post.save")}}" method="post" enctype="multipart/form-data">
                    @csrf()
                    <div class="form-group">
                        <input type="text" placeholder="Content" name="content" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection

