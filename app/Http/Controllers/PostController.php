<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PostController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware("auth");
    }

    public function index()
    {
        $posts = Post::all();
        return view("post.index")->with(["posts" => $posts]);
    }

    public function create()
    {
        return view("post.create");
    }

    public function save(Request $request)
    {
        $user = Auth::user();
        $inputs = $request->only(["content"]);

        $this->validate($request, ["content" => "string|min:6|max:20"]);
        $post = new Post($inputs);
        $post->user()->associate($user);
        $post->save();
        return redirect("/posts");
    }

    public function edit($id, Request $request)
    {
        /** @var Post $post */
        $post = Post::where($id);
        if ($post) {
            throw new NotFoundHttpException("Post not found");
        }
        $content = $request->input("content");
        $post->content = $content;
        $post->user()->associate(Auth::user());
        $post->save();
        return redirect("/posts");
    }
}
